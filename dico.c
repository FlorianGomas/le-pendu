#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <time.h>
#include "fonction.h"
#include "dico.h"

char *pickWord (){

    char *wordPicks = malloc(100*sizeof(char)); // on alloue de la place en m�moire et on stock l'adresse dans se pointeur.
    int nLines = 0;
    char cursor [100] =""; //Pour nous d�placer dans le fichier
    int stopLine = 0;

    FILE* list = fopen("dico.txt", "r"); //On cr�er un pointeur list qui va nous servir pour manipuler notre fichier "dico".
    if (list != NULL){

        nLines = countLine(list); // on compte le nombre de ligne du fichier
        int lineWord = (rand() % (nLines - 0 + 1)) + 0; // on selectionne un nombre al�atoire entre 0 et le nombre de ligne max du fichier

        do{ // Ici on d�place le curseur du nombre de ligne s�lectionner
            fgets(cursor,100,list);
            stopLine++;

        }while (stopLine<lineWord);

        fgets(wordPicks,100,list); // on r�cup�re le mot � la position al�atoirement d�finie
        fclose(list);
     }
    else{
        printf("Impossible d'ouvrir le fichier dico.txt\n");
    }

    return wordPicks;

}


int countLine(FILE *fichier){ // fonction pour compter le nombre de ligne du fichier et ainsi connaitre le nombre de mot dans le dictionnaire.

int c;
int nlines=0;

while((c=fgetc(fichier)) != EOF){

    if(c=='\n'){

        nlines++;
    }

}
rewind(fichier);

return nlines;

}

void delReturn (char *word){ //Fonction pour supprimer le retour � la ligne du mot extrait du fichier

    int numberLetter = strlen(word);
    int i;

    for (i=0; i<numberLetter;i++){

       if(word[i] =='\n'){
        word [i] = '\0';
       }

    }
}


