#ifndef FONCTION_H_INCLUDED
#define FONCTION_H_INCLUDED

char readChar();
void hideWord (char *word);
int letterFound(const char *secretWord, char *wordCopy, char letter, int count);

#endif // FONCTION_H_INCLUDED
