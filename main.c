#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <time.h>
#include "fonction.h"
#include "dico.h"


int main()
{

 srand(time(NULL));
 int gameLoop = 0;
 char *secretWord; // mot � trouver

 do{


    secretWord = pickWord();

    delReturn(secretWord); // on supprime le retour � la ligne du mot choisi dans le fichier dico

    char wordCopy [100] = {0}; // Copie du mot � trouver qui sera "cach�e" puis afficher dans la console
    int count = 10;
    int compare = 1;

    strcpy(wordCopy,secretWord);
    hideWord(wordCopy); // on remplace les lettres du mot par un symbole pour le masquer



    printf("\n=== Bienvenu dans le jeu du pendu ===");


   do{ //boucle de jeu


    printf("\n\nIl vous reste %d coups a jouer.\n", count);
    printf("Quel est le mot secret : %s", wordCopy);
    printf("\nProposez une lettre : ");

    char myLetter = readChar(); // on lit puis stock la lettre saisie dans la variable myLetter
    count = letterFound(secretWord, wordCopy, myLetter,count); //On v�rifie si l'utilisateur � trouv� ou non une des lettres du mot � trouver. Le cas �ch�ant on affiche le mot modifi� � l'�cran.
    compare = strcmp(wordCopy, secretWord);

   }while (compare != 0 && count >0);


   if (count ==0){
    printf("\n\nVous avez perdu !! Le mot a trouve etait : %s\n", secretWord);
   }
   else{
        printf("\n\nVous avez gagne ! Le mot secret etait : %s\n", secretWord);
   }

   free(secretWord);

   printf("\n\nVoulez-vous demarrer une nouvelle partie ?\n\n");
   printf("0-non \n1-oui\n");
   scanf("%d",&gameLoop);
   cleanBuffer();

 }while (gameLoop!=0);

    return 0;
}
