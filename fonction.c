#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <time.h>
#include "fonction.h"
#include "dico.h"

char readChar(){

    char letter = 0 ;

    letter = getchar(); // lecture du premier caract�re.
    letter = toupper(letter); //Mise en majuscule du caract�re.

    while(getchar() != '\n'); // On lit les autres caract�res m�moris�s un � un jusqu'au \n pour les effacer.

    return letter; //On renvoit la lettre lu.

}

void cleanBuffer()
{
    int c = 0;
    while (c != '\n' && c != EOF)
    {
        c = getchar();
    }
}

void hideWord (char *word){

    int numberLetter = strlen(word); // on calcul la longueur du mot � trouver
    int i;

    for (i=0; i<numberLetter;i++){

       word [i] = '*'; //On remplace chaques lettres du mot � trouver par un symbole pour le masquer au joueur
    }


}

int letterFound(const char *secretWord, char *wordCopy, char letter, int count){

   int numberLetter = strlen(secretWord); //on calcul la longueur du mot � trouver
   int i;
   int found = 0; // variable qui nous servira � v�rifier si une lettre � �t� trouv�

    for (i=0; i<numberLetter;i++){ // on parcours le mot

       if (secretWord[i] == letter){  // si une des lettres du mots (ou plsr) sont similaire � la lettre saisie par le joueur ...

            wordCopy[i] = secretWord[i]; // ... on copie la lettre trouv� dans le mot cach�.
            found = 1;
            }
    }

        if (found==0){

           count--;
        }

    return count;

}





